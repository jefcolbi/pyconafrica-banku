from django.utils.translation import gettext as _
from django.utils.formats import date_format
from django.conf import settings
from ninagram.state import *
from ninagram.response import *
from ninagram.inputs import *
from base.models import *
from ninagram.models import *
from loguru import logger
import traceback
import re
import datetime
from ninagram.response import *
from threading import Thread
from django.utils import translation

AVAIL_LANGS = getattr(settings, "AVAIL_LANGS", ['en', 'ru', 'uz'])


@register_state
class StartState(State):
    
    name = "START"
    transitions = {'start':':1', 'user':'USER'}
    
    def pre_next(self, update:telegram.Update):
        try:
            if self.text and self.text.lower() == "make_me_super":
                self.make_me_super()
                return NextResponse(self.name, 1, force_return=True)
                
            return super().pre_next(update)
        except Exception as e:
            logger.exception(str(e))
        
    @register_step    
    def step_1_start_menu(self, update:telegram.Update):
        message = _("Welcome to Pycon Banku restaurant!\n\n")
        message += _("Price: GHC 2")
        replies = [[InlineKeyboardButton(_("Order now"), callback_data="many")],
                   [InlineKeyboardButton(_("Leave feedback"), callback_data='feedback')],
                   [InlineKeyboardButton(_("Lang 🇬🇳🇫🇷🇬🇧"), callback_data='lang')]]
        
        logger.info("is_superuser {}", update.db.user.dj.is_superuser)
        if update.db.user.dj.is_superuser:
            replies.append([InlineKeyboardButton(_("User management"), callback_data='user')])
            
        kbd = InlineKeyboardMarkup(replies)
        
        self.dispatcher.bot.send_photo(update.db.chat.id, "AgADBAADlK4xG0dcYVJyo4OCLKxUn47_sBoABAEAAwIAA3gAAz_xAwABFgQ")
        return MenuResponse(message, kbd, edit_inline_callback=False)
            
    @register_step
    def step_2_lang_menu(self, update:telegram.Update):
        try:
            new_lang = self.get_run('new_lang', None)
            if not new_lang:
                replies = []
                replies.append([InlineKeyboardButton(_("English"), callback_data="english")])
                replies.append([InlineKeyboardButton(("Francais"), callback_data="french")])
                replies.append([InlineKeyboardButton(("Akan"), callback_data="akan")])
                kbd = InlineKeyboardMarkup(replies)
                
                message = "Please select your language\n"
                message += "Choisissez votre langue" #_("Please select your language in russian\n")
                message += "Select your language in Akan" # _("Please select your language in uzbek")
            else:
                replies = [[InlineKeyboardButton(_("OK"), callback_data="start")]]
                kbd = InlineKeyboardMarkup(replies)
                message = _("You selected the lang: {}").format(self.text)
                self.set_run('new_lang', None)
            
            return MenuResponse(message, kbd)
        except Exception as e:
            logger.exception(str(e))
            
    def step_2_next(self, update:telegram.Update):
        try:
            done = False
            
            if self.text.lower() == 'english':
                update.db.chat.lang = "en"
                done = True
            elif self.text.lower() == 'french':
                update.db.chat.lang = "fr"
                done = True
            elif self.text.lower() == 'akan':
                update.db.chat.lang = "gh"
                done = True
                
            if done:                
                # save_this(update.db.chat, cached=True)
                update.db.chat.save()
                self.set_cache(update.db.chat.__class__.__name__, 
                        update.db.chat.id, update.db.chat)
                self.set_run('new_lang', True)
                self.set_run('lang_set', True)
                
                try:
                    lang = update.db.chat.lang
                    translation.activate(lang)
                except:
                    traceback.print_exc()
                    pass                
            return NextResponse(self.name, force_return=True)
        except Exception as e:
            logger.exception(str(e))
            
    @register_step
    def step_3_many_menu(self, update:telegram.Update):
        message = _("How many Banku do you want ?")
        error = self.get_error()
        if error:
            message += _("\n\nError: {}").format(error)
        return MenuResponse(message)
    
    def step_3_many_next(self, update:telegram.Update):
        logger.info("many {}", self.text)
        if self.text.isnumeric():
            self.set_run("number", int(self.text))
            return NextResponse(self.name, 'contact')
        else:
            self.set_error("Input a number please!")
            return NextResponse(self.name)
            
    @register_step
    def step_4_contact_menu(self, update:telegram.Update):
        message = _("Send us your number please!")
        replies = [[telegram.KeyboardButton(_("Tap here"), request_contact=True)]]
        kbd = telegram.ReplyKeyboardMarkup(replies, resize_keyboard=True, one_time_keyboard=True)
        return MenuResponse(message, kbd)
    
    def step_4_contact_next(self, update:telegram.Update):
        if update.message.contact:
            phone_number = update.message.contact.phone_number
            self.set_run('phone', phone_number)
            return NextResponse(self.name, 'location')
        
    @register_step
    def step_5_location_menu(self, update:telegram.Update):
        message = _("Send us your location please!")
        replies = [[telegram.KeyboardButton(_("Tap here"), request_location=True)]]
        kbd = telegram.ReplyKeyboardMarkup(replies, resize_keyboard=True, one_time_keyboard=True)
        return MenuResponse(message, kbd)
    
    def step_5_location_next(self, update:telegram.Update):
        if update.message.location:
            long = update.message.location.longitude
            lat = update.message.location.latitude
            self.set_run('lat', lat)
            self.set_run('long', long)
            return NextResponse(self.name, 'done')
        
    @register_step
    def step_6_done_menu(self, update:telegram.Update):
        self.do_place_order(update)
        message = _("Thanks. An agent will bring you your Banku in few minutes!\n\n")
        message += _("It will cost you GHC{}").format(self.get_run('number', 0)*2)
        replies = [[InlineKeyboardButton(_("Back to the main menu"), callback_data='start')]]
        kbd = InlineKeyboardMarkup(replies)
        return MenuResponse(message, kbd)
    
    @register_step
    def step_7_feedback_menu(self, update:telegram.Update):
        feedback_ok = self.get_run('feedback_ok', False)            
        
        if not feedback_ok:
            message = _("Please send us your suggestion or feedback.")
            
            error = self.get_error()
            if error:
                message += _("\n\nError: {}").format(error)
                
            replies = [[InlineKeyboardButton(_("Back to main menu"), callback_data="start")]]
            kbd = InlineKeyboardMarkup(replies)
        else:
            message = _("Thanks for give us your suggestion/feedback today.")
            replies = [[InlineKeyboardButton(_("Back to main menu"), callback_data="start")]]
            kbd = InlineKeyboardMarkup(replies)
            
        return MenuResponse(message, kbd)
    
    def step_7_feedback_next(self, update:telegram.Update):
        if self.text:
            try:
                Feedback.objects.get(user=update.db.user, date=datetime.date.today())
                self.set_error(_("You already send your feedback today"))
            except:
                traceback.print_exc()
                Feedback.objects.create(content=self.text, user=update.db.user,
                            date=datetime.date.today())
                self.set_run("feedback_ok", True)
            
        return NextResponse(self.name)    
    
    def do_place_order(self, update):
        number = self.get_run('number', None)
        phone = self.get_run('phone', None)
        long = self.get_run('long', None)
        lat = self.get_run('lat', None)
        
        message = _('We received new command\n\n')
        message += _("User: [{}](tg://user?id={}) ")\
            .format(update.db.user.dj.first_name, update.db.user.id)
        if update.db.user.dj.last_name:
            message += _("{} ").format(update.db.user.dj.last_name)
        if update.db.user.dj.username:
            message += _("@{} ").format(update.db.user.dj.username)
        message += "\n"
        message += _("Phone number: {}\n").format(phone)
        message += _("Number of Banku: {}\n").format(number)
        
        for user in User.objects.filter(is_staff=True):
            self.dispatcher.bot.send_message(user.tg.id, message)
            self.dispatcher.bot.send_location(user.tg.id, long, lat)
    


            
@register_state
class UserState(AbstractState):
    
    name = "USER"
    transitions = {'done':':1', 'back':"START", 'start':'START'}
    authorization_instances = {"all":[UserIsStaff()]}
    
    rgx_cb_data = re.compile(r"(\w+)([\+-=])(\w+)\((\w+)\)")
    
    def pre_menu(self, update:telegram.Update):
        pass
    
    def pre_next(self, update:telegram.Update):
        try:
            logger.info("check this")
            text = update.message.text if update.message!=None else update.callback_query.data
            
            if text.lower() == "done":
                self.set_run('user', None)
                self.set_run('keyword', None)
            
            res = self.next_from_class_data(update)
            logger.debug("res is {}", res)
            if res:
                self.set_return(True)
                return res
            
        except Exception as e:
            logger.exception(str(e))
    
    def step_1_menu(self, update:telegram.Update):
        try:
            if self.text is None:
                return MenuResponse("")
            
            
            logger.debug("text {}", self.text)
            replies = []
            message = ""
            
            # check if a search started
            user = self.get_run("user", None)
            keyword = self.get_run('keyword', None)
            if user is None:
                if keyword is None:
                    users = [user.username for user in User.objects.exclude(username="self")]
                    users_str = " ; ".join(users)
                    message = _("Here is the list of all users, i know:\n\n")
                    message += users_str
                    message += _("\n\nSend the username to manage:")
                    replies = [[InlineKeyboardButton(_("Back"), callback_data="start")]]
                    kbd = InlineKeyboardMarkup(replies)
                    return MenuResponse(message, kbd)
                else:
                    self.set_run('keyword', None)
                    try:
                        user = User.objects.get(username=keyword)
                        self.set_run("user", user)
                    except:
                        message = _("This user was not found. Retry!")
                        replies = [[InlineKeyboardButton(_("Back"), callback_data="start")]]
                        kbd = InlineKeyboardMarkup(replies)
                        self.set_return(True)
                        return MenuResponse(message, kbd)
                    
            message = _("Give privileges to user @{}:\n").format(user.username)
            try:            
                if user.is_staff:
                    staff_txt = _("Remove staff")
                    cb_data = "is_staff=bool(false)"
                else:
                    staff_txt = _("Make staff")
                    cb_data = "is_staff=bool(true)"
                    
                if user.is_superuser:
                    super_txt = _("Remove Super")
                    sp_data = "is_superuser=bool(false)"
                else:
                    super_txt = _("Make Super")
                    sp_data = "is_superuser=bool(true)"                    
                    
                replies.append([InlineKeyboardButton(staff_txt, callback_data=cb_data)])
                replies.append([InlineKeyboardButton(super_txt, callback_data=sp_data)])
                    
                replies.append([InlineKeyboardButton(_("Back"), callback_data="done")])
                kbd = InlineKeyboardMarkup(replies)
                    
                return MenuResponse(message, kbd)
            except Exception as e:
                logger.exception(str(e))
                
        except Exception as e:
            logger.exception(str(e))
    
    def step_1_next(self, update:telegram.update):
        try:
            res = self.next_from_class_data(update)
            if res:
                return res
            
            if self.text.lower() == "done":
                self.set_run("user", None)
                self.set_run("keyword", None)
                return NextResponse(self.name)
            
            res = self.rgx_cb_data.search(self.text)
            if res:
                logger.debug("we got a match {}", res.groups())
                field = res.group(1)
                operator = res.group(2)
                fd_type = res.group(3).lower()            
                value = res.group(4)
                
                user = self.get_run("user", None)
                if user:                        
                    if field == "is_staff" and fd_type == "bool":
                        logger.info("value is {}", value.lower())
                        if value.lower() == "true":
                            value = True
                        elif value.lower() == "false":
                            value = False
                        else:
                            raise ValueError("value of a bool type can only be true or false")
                        user.is_staff = value
                        
                    if field == "is_superuser" and fd_type == "bool":
                        if update.db.user.is_superuser:
                            logger.info("value is {}", value.lower())
                            if value.lower() == "true":
                                value = True
                            elif value.lower() == "false":
                                value = False
                            else:
                                raise ValueError("value of a bool type can only be true or false")
                            user.is_superuser = value
                        
                    save_this(user, cached=True)    
                    self.set_run('keyword', user.username)
                    return NextResponse(self.name)
            else:
                logger.debug("No match for the regex found")
            
            self.set_run('keyword', self.text)
        except Exception as e:
            logger.exception(str(e))
            
        return NextResponse(self.name)
    
    def pre_post(self, update:telegram.Update, tg_message:telegram.Message):    
        if update.message and update.message.text:
            try:
                self.dispatcher.bot.delete_message(update.effective_chat.id, 
                                                   update.effective_message.message_id)
            except:
                pass

            try:
                self.dispatcher.bot.delete_message(update.effective_chat.id, 
                                                   update.effective_message.message_id-1)
            except:
                pass
