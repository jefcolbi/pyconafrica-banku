from django.db import models
from ninagram.models import *


class Feedback(models.Model):
    
    content = models.TextField()
    user = models.ForeignKey(TgUser, models.DO_NOTHING)
    date = models.DateField()
